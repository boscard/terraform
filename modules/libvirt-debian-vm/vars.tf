variable "pool_name" {
  type = string
  description = "Pool name"
}

variable "pool_path" {
  type = string
  description = "Path to volume"
}

variable "source_image" {
  type = string
  description = "Source image for main virtual disk"
}

variable "vm_mem" {
  type = string
  description = "Memory for VM in MB"
}

variable "vm_vcpu" {
  type = string
  description = "Numbers of vcpu to assign to vm"
}

variable "network_name" {
  type = string
  description = "Network name to assign to vm"
}

variable "vm_name" {
  type = string
  description = "VM name to create"
}