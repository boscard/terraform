# We fetch the latest ubuntu release image from their mirrors
resource "libvirt_volume" "main_qcow2" {
  name   = "${var.vm_name}.main.qcow2"
  pool   = var.pool_name
  source = var.source_image
  format = "qcow2"
}

data "template_file" "user_data" {
  template = file("${path.module}/files/cloud_init.cfg")
}

data "template_file" "network_config" {
  template = file("${path.module}/files/network_config.cfg")
}

# for more info about paramater check this out
# https://github.com/dmacvicar/terraform-provider-libvirt/blob/master/website/docs/r/cloudinit.html.markdown
# Use CloudInit to add our ssh-key to the instance
# you can add also meta_data field
resource "libvirt_cloudinit_disk" "commoninit" {
  name           = "${var.vm_name}.commoninit.iso"
  user_data      = data.template_file.user_data.rendered
  network_config = data.template_file.network_config.rendered
  pool           = var.pool_name
}

# Create the machine
resource "libvirt_domain" "domain" {
  name   = var.vm_name
  memory = var.vm_mem
  vcpu   = var.vm_vcpu

  cloudinit = libvirt_cloudinit_disk.commoninit.id

  network_interface {
    network_name = var.network_name
  }

  # IMPORTANT: this is a known bug on cloud images, since they expect a console
  # we need to pass it
  # https://bugs.launchpad.net/cloud-images/+bug/1573095
  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  console {
    type        = "pty"
    target_type = "virtio"
    target_port = "1"
  }

  disk {
    volume_id = libvirt_volume.main_qcow2.id
  }

  graphics {
    type        = "spice"
    listen_type = "address"
    autoport    = true
  }

		xml {
	  	xslt = templatefile("${path.module}/templates/disk.xsl",
			  {
				pool_path = var.pool_path,
			    volumes = ["${var.vm_name}.main.qcow2"]
				})
  	}
}
