# Libvirt Debian VM

Create libvirt VM on Debian host

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_libvirt"></a> [libvirt](#requirement\_libvirt) | 0.6.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_libvirt"></a> [libvirt](#provider\_libvirt) | 0.6.3 |
| <a name="provider_template"></a> [template](#provider\_template) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [libvirt_cloudinit_disk.commoninit](https://registry.terraform.io/providers/dmacvicar/libvirt/0.6.3/docs/resources/cloudinit_disk) | resource |
| [libvirt_domain.domain](https://registry.terraform.io/providers/dmacvicar/libvirt/0.6.3/docs/resources/domain) | resource |
| [libvirt_volume.main_qcow2](https://registry.terraform.io/providers/dmacvicar/libvirt/0.6.3/docs/resources/volume) | resource |
| [template_file.network_config](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |
| [template_file.user_data](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_network_name"></a> [network\_name](#input\_network\_name) | Network name to assign to vm | `string` | n/a | yes |
| <a name="input_pool_name"></a> [pool\_name](#input\_pool\_name) | Pool name | `string` | n/a | yes |
| <a name="input_pool_path"></a> [pool\_path](#input\_pool\_path) | Path to volume | `string` | n/a | yes |
| <a name="input_source_image"></a> [source\_image](#input\_source\_image) | Source image for main virtual disk | `string` | n/a | yes |
| <a name="input_vm_mem"></a> [vm\_mem](#input\_vm\_mem) | Memory for VM in MB | `string` | n/a | yes |
| <a name="input_vm_name"></a> [vm\_name](#input\_vm\_name) | VM name to create | `string` | n/a | yes |
| <a name="input_vm_vcpu"></a> [vm\_vcpu](#input\_vm\_vcpu) | Numbers of vcpu to assign to vm | `string` | n/a | yes |

## Outputs

No outputs.
