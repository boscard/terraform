<?xml version="1.0" ?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output omit-xml-declaration="yes" indent="yes"/>
  <xsl:template match="node()|@*">
     <xsl:copy>
       <xsl:apply-templates select="node()|@*"/>
     </xsl:copy>
  </xsl:template>

  <xsl:template match="/domain/devices/disk[@device='disk']/@type">
    <xsl:attribute name="type">
      <xsl:value-of select="'file'"/>
    </xsl:attribute>
  </xsl:template>
  <xsl:template match="/domain/devices/disk[@device='disk']/source">
    <xsl:choose>
    ${join("\n",flatten([for volume_name in volumes:
      "<xsl:when test=\"@volume = '${volume_name}'\">\n          <source file='${pool_path}/${volume_name}' />\n        </xsl:when>"
    ]))}
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>

